# Hi there, I'm Karthi 👋

I'm a generalist innovator,currently working with BSGW on a software utility and developing a healthcare project with CII's mentorship program. I'm also a prefinal year B.Tech IT student in KCT.

- 🔭 I’m currently working on:
  - A Translator Utility Project with Bosh Global Software Technology 🌐
  - A Healthcare Project with CII's Mentorship Program 🏥
  - A Solution to Make e-Sanjeevani Application Developed by Ministry of Health India More Accessible to Rural People Using a Kiosk Application 🚑
- 🌱 I’m currently learning:
  - WinUI3 Windows Native Application Development 💻
  - AI 🧠
- 👯 I’m looking to collaborate with you all to create great things with you 🙌
  
&emsp;&emsp;&emsp;&emsp; <a href="https://www.linkedin.com/in/karthidreamr/"><img src="https://logospng.org/download/linkedin/logo-linkedin-icon-4096.png" width="50" height="50" /></a>
&ensp;
<a href="https://stackexchange.com/users/25128898/karthidreamr"><img src="https://cdn2.iconfinder.com/data/icons/social-icons-color/512/stackoverflow-1024.png" width="50" height="50" /></a>  &ensp;
<a href="https://twitter.com/KarthiDreamr"><img src="twitter_white_small.png" width="43" height="46" /></a>
&ensp;
<a href="https://www.reddit.com/user/KarthiDreamr"><img src="https://www.elementaryos-fr.org/wp-content/uploads/2019/08/logo-reddit-600x600.png" width="50" height="50" /></a>
&ensp;
<a href="mailto:karthidreamr@gmail.com"><img src="https://logos-world.net/wp-content/uploads/2020/11/Gmail-Logo.png" width="88" height="50" /></a> 
- 🤔 I’m looking for help with learning more about Unity and game development 🎮
- 💬 Ask me about anything related to Android, Flutter, MySQL, Tkinter, Django, or UI/UX design 📱
- ⚡ Fun fact: <br>
   _Did you know that the first computer "bug" was actually a real bug? In 1947, a moth got trapped in a relay of a Mark II computer at Harvard University. Hence, the term "debugging" was born! 🐞_
- 😎 More about me: <br>
  _I'm a fan of sci-fi movies 🎥, animated YouTube videos 🎞️, Harry Potter ⚡ and science 🔬. Walker ID 85449 :guitar: . I'm an optimistic nihilist, supporter of humanity and justice. I want to make the world more sustainable technology 🌎._
  
</br>
<div align="center">
  
![](https://komarev.com/ghpvc/?username=KarthiDreamr&color=blueviolet)

</div>

 ## Tech Stack

<div align="center">

  ### Langs
![C](https://img.shields.io/badge/c-%2300599C.svg?style=flat&logo=c&logoColor=white) ![C#](https://img.shields.io/badge/c%23-%23239120.svg?style=flat&logo=c-sharp&logoColor=white) ![C++](https://img.shields.io/badge/c++-%2300599C.svg?style=flat&logo=c%2B%2B&logoColor=white) ![CSS3](https://img.shields.io/badge/css3-%231572B6.svg?style=flat&logo=css3&logoColor=white) ![Dart](https://img.shields.io/badge/dart-%230175C2.svg?style=flat&logo=dart&logoColor=white) ![HTML5](https://img.shields.io/badge/html5-%23E34F26.svg?style=flat&logo=html5&logoColor=white) ![JavaScript](https://img.shields.io/badge/javascript-%23323330.svg?style=flat&logo=javascript&logoColor=%23F7DF1E) ![Kotlin](https://img.shields.io/badge/kotlin-%230095D5.svg?style=flat&logo=kotlin&logoColor=white) ![Python](https://img.shields.io/badge/python-3670A0?style=flat&logo=python&logoColor=ffdd54) ![Shell Script](https://img.shields.io/badge/shell_script-%23121011.svg?style=flat&logo=gnu-bash&logoColor=white)

  ### Services
![Firebase](https://img.shields.io/badge/firebase-%23039BE5.svg?style=flat&logo=firebase)

  ### Frameworks/Libraries
![Bootstrap](https://img.shields.io/badge/bootstrap-%23563D7C.svg?style=flat&logo=bootstrap&logoColor=white) ![Chakra](https://img.shields.io/badge/chakra-%234ED1C5.svg?style=flat&logo=chakraui&logoColor=white) ![Express.js](https://img.shields.io/badge/express.js-%23404d59.svg?style=flat&logo=express&logoColor=%2361DAFB)  ![Flutter](https://img.shields.io/badge/Flutter-%2302569B.svg?style=flat&logo=Flutter&logoColor=white) ![NPM](https://img.shields.io/badge/node.js-6DA55F?style=flat&logo=node.js&logoColor=white) ![React](https://img.shields.io/badge/react-%2320232a.svg?style=flat&logo=react&logoColor=%2361DAFB) ![React Router](https://img.shields.io/badge/svelte-%23f1413d.svg?style=flat&logo=svelte&logoColor=white) ![TailwindCSS](https://img.shields.io/badge/tailwindcss-%2338B2AC.svg?style=flat&logo=tailwind-css&logoColor=white) 

  ### Design
![Figma](https://img.shields.io/badge/figma-%23F24E1E.svg?style=flat&logo=figma&logoColor=white) ![Inkscape](https://img.shields.io/badge/Inkscape-e0e0e0?style=flat&logo=inkscape&logoColor=080A13) ![Gimp Gnu Image Manipulation Program](https://img.shields.io/badge/Gimp-657D8B?style=flat&logo=gimp&logoColor=FFFFFF) 

<!--  ### AI/ML
![TensorFlow](https://img.shields.io/badge/TensorFlow-%23FF6F00.svg?style=flat&logo=TensorFlow&logoColor=white)    -->

  ### Others
![CMake](https://img.shields.io/badge/CMake-%23008FBA.svg?style=flat&logo=cmake&logoColor=white) ![Gradle](https://img.shields.io/badge/Gradle-02303A.svg?style=flat&logo=Gradle&logoColor=white) ![ESLint](https://img.shields.io/badge/ESLint-4B3263?style=flat&logo=eslint&logoColor=white)
  
  </div>

## Stats and Streaks

<div align="center">
  
  ![Karthi's GitHub stats](https://github-readme-stats.vercel.app/api?username=karthidreamr&show_icons=true&hide_rank=true&show=prs_merged,prs_merged_percentage&theme=github_dark) &nbsp; &nbsp;
  ![KarthiDreamr's Streak](http://github-readme-streak-stats.herokuapp.com?user=KarthiDreamr&theme=github-dark&date_format=j%20M%5B%20Y%5D&border=FFFFFF&ring=4C8EDA&stroke=FFFFFF&dates=1D64D0)

  <!-- ![Karthi's Github Streak🔥 ](https://github-readme-streak-stats.herokuapp.com/?user=KarthiDreamr&theme=github-dark) -->
  ![KarthiDreamr's Trophies](https://github-profile-trophy.vercel.app/?username=KarthiDreamr&rank=-B&column=-1&no-frame=true&margin-w=10)  
  </br>
  ![KarthiDreamr's language stats](https://github-readme-stats.vercel.app/api/top-langs/?username=KarthiDreamr&theme=github_dark)
    
</div>

<!-- ## Top Languages

![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=karthidreamr&theme=github_dark) -->


## My Recent Projects

<div align="center">

[![Readme Card](https://github-readme-stats.vercel.app/api/pin/?username=karthidreamr&repo=DashNotes&theme=github_dark)](https://github.com/karthidreamr/DashNotes)

[![Readme Card](https://github-readme-stats.vercel.app/api/pin/?username=karthidreamr&repo=Graminconnect&theme=github_dark)](https://github.com/karthidreamr/Graminconnect)

[![Readme Card](https://github-readme-stats.vercel.app/api/pin/?username=karthidreamr&repo=UCare&theme=github_dark)](https://github.com/karthidreamr/UCare)

[![Readme Card](https://github-readme-stats.vercel.app/api/pin/?username=karthidreamr&repo=GyroSensing-Android-Jetpack&theme=github_dark)](https://github.com/karthidreamr/IT-KCT-Modern-Android-Development)

</div>
